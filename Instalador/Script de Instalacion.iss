[Setup]
AppName=Proyecto
AppVerName=Proyecto
DefaultGroupName=Proyecto
AppPublisher=Proyecto
AppVersion=1.0
AllowNoIcons=false
AppCopyright=
PrivilegesRequired=admin
; Este es el nombre del archivo exe que se va a generar
OutputBaseFilename=ProyectoSetup
; Esta es la carpeta de instalaci�n por defecto. OJO: {pf} es una variable propia de
; innosetup y significa la carpeta de Archivos de programa (o Program files si es
; un windows en ingl�s)
DefaultDirName={pf}\Proyecto
[Languages]
Name: "spanish"; MessagesFile: "compiler:Languages\Spanish.isl"

[Tasks]
; Esto no se toca. Es la indicaci�n para innosetup de que debe crear los �conos necesarios
; para iniciar el programa y para desinstalarlo
Name: desktopicon; Description: Create a &desktop icon; GroupDescription: Additional icons:

[Files]
; OJO: antes que todo. Los par�metros: regserver restartreplace shared file, etc. son
; par�metros que tienen que ir tal y como aparecen ac�. Cuesta un poco comprenderlos.
; Por ahora los dejamos tal y como est�n ac�.
; Otra cosa: {sys} = carpeta system de windows
;            {win} = carpeta windows de windows
;            {cf} = carpeta archivos comunes de windows
;            {tmp} = carpeta temporal de windows
;            {app} = carpeta donde se va a instalar el programa (fue definida arriba en el par�metro: DefaultDirName=
; -------------------------------------------------------------------------------------
; Aqu� van los archivos de la aplicaci�n: el .exe y otros que ocupe el programa
Source: C:\Users\borja\Desktop\proyecto\out\artifacts\proyecto_jar\proyecto.jar; DestDir: {app}; Flags: ignoreversion
Source: C:\Users\borja\Desktop\proyecto\out\artifacts\proyecto_jar\*; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: C:\Users\borja\Desktop\proyecto\out\artifacts\proyecto_jar\script.sql*; DestDir: {tmp}; Flags : ignoreversion deleteafterinstall


[INI]

[Icons]
; Estos son los �conos que el instalador va a crear en el grupo de programas.
; Aqu� se incluye: el �cono para abrir el programa, el �cono para desinstalar el programa
; y el �cono que se ubica en el escritorio
; OJO: {group} = nombre del grupo de programa que se defini� arriba en el par�metro: DefaultGroupName=
Name: {group}\Proyecto; Filename: {app}\Proyecto.jar; WorkingDir: {app}; IconIndex: 0
Name: {group}\Desinstalar Proyecto; Filename: {uninstallexe}
Name: {userdesktop}\Proyecto; Filename: {app}\Proyecto.jar; Tasks: desktopicon; WorkingDir: {app}; IconIndex: 0

[Run]

; Instalamos MySQL 5 en modo desatendido y silencioso
Filename: msiexec; Parameters: "/i mysql-5.5.39-winx64.msi /qn INSTALLDIR=""C:\mysql"""; WorkingDir: {src}\complementos; StatusMsg: Instalando Motor de Base de Datos; Description: Instalar Motor de Base de Datos; Flags: runhidden
; Instalamos el servicio de MySQL
;C:\mysql\bin\
Filename: C:\mysql\bin\mysqld.exe; Parameters: --install; WorkingDir: C:\mysql\bin; StatusMsg: Instalando Servicio MySQL; Description: Instalar Servicio MySQL; Flags: runhidden
; Levantamos el servicio de MySQL en Windows 
Filename: net.exe; Parameters: start mysql; StatusMsg: Iniciando Servicio MySQL; Description: Iniciar Servicio MySQL; Flags: runhidden
; Insertamos al usuario micelanea
Filename: C:\mysql\bin\mysql.exe; Parameters: "-e ""insert into mysql.user(host,user,password) values ('localhost','proyecto',PASSWORD('proyecto'));"" -u root"; WorkingDir: {tmp}; StatusMsg: Configurando Servidor de Base de Datos Creando Usuario; Flags: runhidden
;Esto me permite crear la base de datos
Filename: C:\mysql\bin\mysql.exe; Parameters: "-u root -h localhost -e ""create database proyecto CHARACTER SET 'utf8' COLLATE utf8_spanish_ci";  WorkingDir: {tmp}; StatusMsg: Creando la Base dedatos; Flags: runhidden
; Le damos todos los privilegios al usuario micelanea del localhost
Filename: C:\mysql\bin\mysql.exe; Parameters: "-e ""grant all privileges on proyecto.* to proyecto;"" -u root"; WorkingDir: {tmp}; StatusMsg: Configurando Servidor de Base de Datos Asignando privilegios; Flags: runhidden
; Flusheamos los privilegios
Filename: C:\mysql\bin\mysql.exe; Parameters: "-e ""flush privileges;"" -u root"; WorkingDir: {tmp}; StatusMsg: Configurando Servidor de Base de Datos Flusheando privilegios; Flags: runhidden
;cargamos la base de datos
Filename: C:\mysql\bin\mysql.exe; Parameters: "-u root -h localhost -e ""use proyecto; source script.sql;";  WorkingDir: {tmp}; StatusMsg: Creando Base de Datos; Flags: runhidden
Filename: {src}\complementos\java.exe; WorkingDir: {src}\complementos;

;[UninstallRun]
;Filename: msiexec.exe; Parameters: "/x ""{app}\mysql-connector-odbc-5.2.4-ansi-win32.msi"""
;Filename: msiexec.exe; Parameters: "/x ""{app}\mysql-essential-5.0.45-win32.msi"""

[Messages]
; Estos mensajes simplemente son un override de los mensajes de Innosetup ya que vienen
; en ingl�s.
WelcomeLabel1=Instalaci�n de Punto de venta Proyecto
WelcomeLabel2=Este proceso instalar� Punto de venta Proyecto.%n%nSe recomienda cerrar todas las aplicaciones abiertas%nantes de continuar.


