
create table if not exists productos(
	id int unsigned not null auto_increment primary key,
	tipo varchar(250),
	descripcion varchar(300),
	precio float,
	imagen blob,
	stock int unsigned,
	marca varchar(250),
	modelo varchar(250)
);
create table if not exists empleados(
	id int unsigned not null auto_increment primary key,
	nombre varchar(250),
	apellidos varchar(250),
	telefono varchar(20),
	email varchar(250),
	nick varchar(250),
	tipo varchar(250),
	pass varchar(320)
);
insert into empleados (nick,tipo,pass) values('admin','admin','admin');
create table if not exists clientes(
	id int unsigned not null auto_increment primary key,
	nombre varchar(250),
	apellidos varchar(250),
	dni varchar(250),
	email varchar(250),
	fecha_nacimiento date,
	direccion varchar(250),
	telefono varchar(200)
);
create table if not exists pedidos(
	id int unsigned not null auto_increment primary key,
	id_empleado int unsigned,
	index(id_empleado),
	FOREIGN key(id_empleado)
		references empleados(id)
		on delete set null on update cascade,
	id_cliente int unsigned,
	index(id_cliente),
	FOREIGN key(id_cliente)
		references clientes(id)
		on delete set null on update cascade,
	iva int unsigned,
	fecha date,
	total_total float unsigned,
	total_sin float unsigned,
	forma_pago varchar(50),
	descuento int unsigned,
	numero varchar(500)
);
create table if not exists pedido_producto(
	id int unsigned not null auto_increment primary key,
	id_pedido int unsigned,
	index(id_pedido),
	FOREIGN key(id_pedido)
		references pedidos(id)
		on delete cascade on update cascade,
	id_producto int unsigned,
	index(id_producto),
	FOREIGN key(id_producto)
		references productos(id)
		on delete set null on update cascade,
	precio float unsigned,
	cantidad int unsigned,
	nombre varchar(250)
);
create table if not exists albaranes(
	id int unsigned not null auto_increment primary key,
	fecha date,
	id_pedido int unsigned,
	index(id_pedido),
	FOREIGN key(id_pedido)
		references pedidos(id)
		on delete set null on update cascade,
	numero varchar(500)
);
create table if not exists albaran_producto(
	id int unsigned not null auto_increment primary key,
	id_albaran int unsigned,
	index(id_albaran),
	FOREIGN key(id_albaran)
		references albaranes(id)
		on delete cascade on update cascade,
	id_producto int unsigned,
	index(id_producto),
	FOREIGN key(id_producto)
		references productos(id)
		on delete set null on update cascade,
	precio float unsigned,
	cantidad int unsigned,
	nombre varchar(250)
);
create table if not exists facturas(
	id int unsigned not null auto_increment primary key,
	fecha date,
	id_albaran int unsigned,
	index(id_albaran),
	FOREIGN key(id_albaran)
		references albaranes(id)
		on delete set null on update cascade,
	numero varchar(500)
);
create table if not exists factura_producto(
	id int unsigned not null auto_increment primary key,
	id_factura int unsigned,
	index(id_factura),
	FOREIGN key(id_factura)
		references facturas(id)
		on delete cascade on update cascade,
	id_producto int unsigned,
	index(id_producto),
	FOREIGN key(id_producto)
		references productos(id)
		on delete set null on update cascade,
	precio float unsigned,
	cantidad int unsigned,
	nombre varchar(250)
);


