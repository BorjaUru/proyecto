package com.bulsa.proyecto.base;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import java.util.List;

/**
 * Created by borja on 23/05/2017.
 * rellena los datos de la factura en el repor asignado cada dato con el campo que corresponde
 */
public class DataSourceF implements JRDataSource {
    private List<DetalleFactura> detalleFactura;
    private int indice= -1;
    /**
     * asigna un dato al fiel que le corresponde del report
     * @param jrField el field que toca
     * @return devuelve el valor
     * @throws JRException
     */
    @Override
    public Object getFieldValue(JRField jrField) throws JRException {
        Object valor = null;
        if(jrField.getName().equals("producto")){
            valor =detalleFactura.get(indice).getNombre();
        }else if(jrField.getName().equals("descripcion")){
            String a=detalleFactura.get(indice).getProducto().getDescripcion();
            if (a!=null){
                valor=a;
            }else{
                valor="Sin descripcion";
            }
        }else if(jrField.getName().equals("cantidad")){
            valor=detalleFactura.get(indice).getCantidad();
        }else if(jrField.getName().equals("precio")){
            valor=detalleFactura.get(indice).getPrecio();
        }else if(jrField.getName().equals("importe")){
            valor=detalleFactura.get(indice).getPrecio()*detalleFactura.get(indice).getCantidad();
        }
        return valor;
    }
    /**
     * retorna el incide proximo si es menor que el tamaño del array
     * @return
     * @throws JRException
     */
    @Override
    public boolean next() throws JRException {
        return ++indice < detalleFactura.size();
    }
    /**
     * añade un array para insertar los datos en el report
     * @param detallesFactura
     */
    public void add(List detallesFactura){
        this.detalleFactura=detallesFactura;
    }
}
