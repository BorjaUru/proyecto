package com.bulsa.proyecto.base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by BULSA on 06/04/2017.
 * define los campos de un pedido y el mapeo de hibernate
 */
@Entity
@Table(name="pedidos")
public class Pedido {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @ManyToOne
    @JoinColumn(name="id_empleado")
    private Empleado empleado;
    @ManyToOne
    @JoinColumn(name="id_cliente")
    private Cliente cliente;
    @Column(name="iva")
    private int iva;
    @Column(name="fecha")
    private Date fecha;
    @Column(name="total_total")
    private float tt;
    @Column(name="total_sin")
    private float ts;
    @Column(name="forma_pago")
    private String formaPago;
    @OneToMany(mappedBy = "pedido", cascade = CascadeType.ALL)
    private List<Albaran> albaranes;
    @Column(name="descuento")
    private int descuento;
    @Column(name="numero")
    private String numero;
    @OneToMany(mappedBy = "pedido",cascade = CascadeType.ALL)
    private List<DetallePedido> listaDetallePedido;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getIva() {
        return iva;
    }

    public void setIva(int iva) {
        this.iva = iva;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public float getTt() {
        return tt;
    }

    public void setTt(float tt) {
        this.tt = tt;
    }

    public float getTs() {
        return ts;
    }

    public void setTs(float ts) {
        this.ts = ts;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    public int getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public List<DetallePedido> getListaDetallePedido() {
        return listaDetallePedido;
    }

    public void setListaDetallePedido(List<DetallePedido> listaDellatePedido) {
        this.listaDetallePedido = listaDellatePedido;
    }

    public List<Albaran> getAlbaranes() {
        return albaranes;
    }

    public void setAlbaranes(List<Albaran> albaranes) {
        this.albaranes = albaranes;
    }

    @Override
    public String toString() {
        return  numero ;
    }

    /**
     * añade un albaran al pedido
     * @param a
     */
    public void anadirAlbaran(Albaran a){
        albaranes=new ArrayList<>();
        albaranes.add(a);
    }
}
