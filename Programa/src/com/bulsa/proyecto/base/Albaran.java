package com.bulsa.proyecto.base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by BORJA on 19/04/2017.
 * define un albaran y el mapeo de hibernate
 */
@Entity
@Table(name="albaranes")
public class Albaran {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @ManyToOne
    @JoinColumn(name="id_pedido")
    private Pedido pedido;
    @OneToMany(mappedBy = "albaran", cascade = CascadeType.ALL)
    private List<Factura> facturas;
    @Column(name="numero")
    private String numero;
    @Column(name="fecha")
    private Date fecha;
    @OneToMany(mappedBy = "albaran", cascade = CascadeType.ALL)
    private List<DetalleAlbaran> listaDetalleAlbaran;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public List<Factura> getFacturas() {
        return facturas;
    }

    public void setFacturas(List<Factura> facturas) {
        this.facturas = facturas;
    }



    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public List<DetalleAlbaran> getListaDetalleAlbaram() {
        return listaDetalleAlbaran;
    }

    public void setListaDetalleAlbaran(List<DetalleAlbaran> listaDetalleAlbaran) {
        this.listaDetalleAlbaran = listaDetalleAlbaran;
    }

    /**
     * añade una factura al albaran
     * @param f factura a guardar
     */
    public void anadirFactura(Factura f){
        facturas=new ArrayList<>();
        facturas.add(f);
    }
}
