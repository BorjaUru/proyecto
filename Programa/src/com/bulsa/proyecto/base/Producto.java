package com.bulsa.proyecto.base;

import javax.persistence.*;
import java.util.List;

/**
 * define los atributos de un producto y el mapeo de hibernate
 * Created by BUlSA on 06/04/2017.
 */
@Entity
@Table(name="productos")
public class Producto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="tipo")
    private String tipo;
    @Column(name="descripcion")
    private String descripcion;
    @Column(name="precio")
    private float precio;
    @Lob
    @Column(name = "imagen")
    private byte[] image;
    @Column(name="stock")
    private int stock;
    @Column(name="marca")
    private String marca;
    @Column(name="modelo")
    private String modelo;
    @OneToMany(mappedBy = "pedido", cascade = CascadeType.ALL)
    private List<DetallePedido> listaDetallePedido;
    @OneToMany(mappedBy = "albaran", cascade = CascadeType.ALL)
    private List<DetalleAlbaran> listaDetalleAlbaran;
    @OneToMany(mappedBy = "factura", cascade = CascadeType.ALL)
    private List<DetalleFactura> listaDetalleFactura;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public List<DetallePedido> getListaDetallePedido() {
        return listaDetallePedido;
    }

    public void setListaDetallePedido(List<DetallePedido> listaDellatePedido) {
        this.listaDetallePedido = listaDellatePedido;
    }

    public List<DetalleAlbaran> getListaDetalleAlbaran() {
        return listaDetalleAlbaran;
    }

    public void setListaDetalleAlbaran(List<DetalleAlbaran> listaDetalleAlbaran) {
        this.listaDetalleAlbaran = listaDetalleAlbaran;
    }

    public List<DetalleFactura> getListaDetalleFactura() {
        return listaDetalleFactura;
    }

    public void setListaDetalleFactura(List<DetalleFactura> listaDetalleFactura) {
        this.listaDetalleFactura = listaDetalleFactura;
    }

    @Override
    public String toString() {
        return  marca + " " + modelo;
    }
}
