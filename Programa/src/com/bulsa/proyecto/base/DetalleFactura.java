package com.bulsa.proyecto.base;

import javax.persistence.*;

/**
 * Created by BORJA on 19/04/2017.
 * define el detalle de una factura y el mapeo de hibernate
 */
@Entity
@Table(name="factura_producto")
public class DetalleFactura {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @ManyToOne
    @JoinColumn(name="id_factura")
    private Factura factura;
    @ManyToOne
    @JoinColumn(name="id_producto")
    private Producto producto;
    @Column(name="precio")
    private float precio;
    @Column(name="cantidad")
    private int cantidad;
    @Column(name="nombre")
    private String nombre;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
