package com.bulsa.proyecto.gui;

import com.bulsa.proyecto.base.*;
import com.toedter.calendar.JDateChooser;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;


/**
 * Created by BORJA on 03/04/2017.
 * Clase principal se pinta la ventana, gestiona todos los eventos posibles y gestiona los datos para enviarlos a la base de datos o pintarlos.
 */
public class Ventana  implements MouseListener, ActionListener {
    private JTabbedPane pestanas;
    private JPanel panel1;
    private JTextField eNombre;
    private JList listaEmpleados;
    private JTextField eApellidos;
    private JTextField eNick;
    private JTextField eTelefono;
    private JPasswordField ePass;
    private JComboBox eTipoCombo;
    private JTable pProductosTabla;
    private JList listaPedidos;
    private JTextField pNumero;
    private JTextField pSub;
    private JTextField pTotal;
    private JComboBox pEmpleadoCombo;
    private JComboBox pClienteCombo;
    private JComboBox pDescuentoCombo;
    private JComboBox pIvaCombo;
    private JDateChooser pFecha;
    private JComboBox pFormaCombo;
    private JList listaCLientes;
    private JTextField cDni;
    private JTextField cDireccion;
    private JTextField cTelefono;
    private JTextField cNombre;
    private JTextField cApellidos;
    private JTextField cEmail;
    private JList listaProductos;
    private JTextField prMarca;
    private JTextField prTipo;
    private JTextField prModelo;
    private JTextField prPrecio;
    private JTextField prStock;
    private JTextArea prDescripcion;
    private JButton nuevoButton;
    private JButton editarButton;
    private JButton borrarButton;
    private JButton guardarButton;
    private JButton crearFacturaButton;
    private JButton anadirProductosButton;
    private JButton crearAlbaranButton;
    private JTextField eBuscar;
    private JTextField cBuscar;
    private JTextField prBuscar;
    private JTextField pBucar;
    private JPanel botones;
    private JTextField eEmail;
    private JDateChooser cNacimiento;
    private JLabel fotol;
    private JLabel labelPass;
    private JButton verGrafico;
    private Hibernate hibernate;
    private Empleado usuario;
    private DefaultListModel<Empleado> modelolistaEmpleados;
    private DefaultListModel<Cliente>modeloListaClientes;
    private DefaultListModel<Producto>modelListaProductos;
    private DefaultListModel<Pedido>modeloListaPedidos;
    private boolean nuevo;
    private JPanel pestana;
    private char punto;
    private DefaultTableModel modeloTabla;
    private String temp="";
    private ArrayList<DetallePedido>temporal;
    private boolean k =true;

   public Ventana(){
       pProductosTabla.setEnabled(false);
       hibernate=new Hibernate();
       hibernate.conectar();
       while (usuario==null) {
           Login l = new Login();
           l.setLocationRelativeTo(null);
           l.pack();
           l.setVisible(true);
           if(l.isCancelar()){
               cerrar();
           }

          usuario=hibernate.login(l.getUsuario(),l.getPass());

       }
       inicializarModelosListas();
       inicializarLiseners();
       if(!usuario.getTipo().equals("admin")){
            pestanas.removeTabAt(0);

           for (Cliente c:hibernate.getClientes()) {
               modeloListaClientes.addElement(c);
           }
           if(usuario.getTipo().equals("visualizar")){
                botones.setVisible(false);
           }
       }else {

           for (Empleado e:hibernate.getEmpleados()) {
               modelolistaEmpleados.addElement(e);
           }
       }

        nuevo=false;

        pestana=(JPanel)pestanas.getSelectedComponent();
        cNacimiento.setEnabled(false);
        pFecha.setEnabled(false);
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(this.panel1);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent evt) {
                cerrar();
            }
        });
        frame.setLocationRelativeTo(null);
        frame.setSize(700,400);
        frame.setResizable(false);
        frame.setVisible(true);

   }

    /**
     * asigna los modelos a las listas
     */

    public void inicializarModelosListas(){
        modeloListaClientes=new DefaultListModel<>();
        listaCLientes.setModel(modeloListaClientes);
        if(usuario.getTipo().equals("admin")) {
            modelolistaEmpleados = new DefaultListModel<>();
            listaEmpleados.setModel(modelolistaEmpleados);
        }
        modelListaProductos=new DefaultListModel<>();
        listaProductos.setModel(modelListaProductos);
        modeloListaPedidos=new DefaultListModel<>();
        listaPedidos.setModel(modeloListaPedidos);
    }

    /**
     * inicializa los lisener de los elementos y contiene lo que tienen que hacer
     */
    public void inicializarLiseners(){
        verGrafico.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List <Pedido> lPedidos=hibernate.getPedidoss();
                List lEmpleados=new ArrayList();
                for(Pedido p:lPedidos){
                    lEmpleados.add(p.getEmpleado());
                }

                try {
                    JasperReport report = (JasperReport)JRLoader.loadObjectFromFile("proyecto1.jasper");
                    JasperPrint jasperPrint = JasperFillManager.fillReport(report, null, new JRBeanCollectionDataSource(lEmpleados));
                    JasperViewer.viewReport(jasperPrint,false);
                } catch (JRException e1) {
                    e1.printStackTrace();
                }
            }
        });
        labelPass.addMouseListener(this);
        pestanas.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent ee) {
                nuevoButton.setText("Nuevo");
                crearFacturaButton.setEnabled(false);
                crearAlbaranButton.setEnabled(false);
                temp="";
                nuevo=false;
                limpiar(pestana);
                cambiarEstadosComponetesPanel(false,pestana);
                cambiarEstadoBotoneraD();
                pestana=(JPanel)pestanas.getSelectedComponent();

                switch (pestana.getName()){
                    case "Empleados":
                        modelolistaEmpleados.removeAllElements();
                        for (Empleado e:hibernate.getEmpleados()) {
                            modelolistaEmpleados.addElement(e);
                        }
                        break;
                    case "Productos":
                        modelListaProductos.removeAllElements();
                        for (Producto p:hibernate.getProductos()) {
                            modelListaProductos.addElement(p);
                        }

                        break;
                    case "Pedidos":
                        crearFacturaButton.setName("crear");
                        crearFacturaButton.setText("Crear factura");
                        crearAlbaranButton.setName("crear");
                        crearAlbaranButton.setText("Crear Albaran");
                        verGrafico.setEnabled(true);
                        modeloListaPedidos.removeAllElements();
                        for (Pedido p:hibernate.getPedidos()) {
                            modeloListaPedidos.addElement(p);
                        }
                        pClienteCombo.removeAllItems();
                        pClienteCombo.addItem("");
                        for (Cliente c:hibernate.getClientes()) {
                            pClienteCombo.addItem(c);
                        }
                        pEmpleadoCombo.removeAllItems();
                        pEmpleadoCombo.addItem("");
                        for (Empleado e:hibernate.getEmpleados()) {
                            pEmpleadoCombo.addItem(e);
                        }

                        break;
                    case "Clientes":
                        modeloListaClientes.removeAllElements();
                        for (Cliente c:hibernate.getClientes()) {
                            modeloListaClientes.addElement(c);
                        }
                        break;
                    default:
                        break;
                }


            }
        });
        pIvaCombo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(!pSub.getText().equals("")){
                    calcular();
                }
            }
        });
        pDescuentoCombo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(!pSub.getText().equals("")){
                    calcular();
                }
            }
        });
        nuevoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                crearFacturaButton.setName("crear");
                crearFacturaButton.setText("Crear factura");
                crearAlbaranButton.setName("crear");
                crearAlbaranButton.setText("Crear Albaran");
                temp="";
                limpiar((JPanel) pestanas.getSelectedComponent());
                if(nuevoButton.getText().equals("Nuevo")) {
                    nuevo = true;
                    cambiarEstadosComponetesPanel(true, (JPanel) pestanas.getSelectedComponent());
                    cambiarEstadoBotoneraA();
                    nuevoButton.setText("Cancelar");
                    temp="";
                    if(pestanas.getSelectedComponent().getName().equals("Pedidos")){
                        String a=hibernate.nexNumPedido()+"";
                        pNumero.setText(a);
                        pFecha.setDate(new Date());
                        crearAlbaranButton.setEnabled(false);
                        crearFacturaButton.setEnabled(false);
                        pClienteCombo.removeAllItems();
                        pClienteCombo.addItem("");
                        for (Cliente c:hibernate.getClientes()) {
                            pClienteCombo.addItem(c);
                        }
                        pEmpleadoCombo.removeAllItems();
                        pEmpleadoCombo.addItem("");
                        for (Empleado ee:hibernate.getEmpleados()) {
                            pEmpleadoCombo.addItem(ee);
                        }
                        verGrafico.setEnabled(false);
                    }


                }else{
                    nuevo = false;
                    cambiarEstadosComponetesPanel(false, (JPanel) pestanas.getSelectedComponent());
                    cambiarEstadoBotoneraD();
                    nuevoButton.setText("Nuevo");
                    verGrafico.setEnabled(true);
                }

            }
        });
        editarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                crearAlbaranButton.setEnabled(true);
                cambiarEstadoBotoneraB();
                editarButton.setEnabled(false);
                nuevoButton.setEnabled(true);
                nuevoButton.setText("Cancelar");
                JPanel p=(JPanel)pestanas.getSelectedComponent();
                if(p.getName().equals("Pedidos")){
                    Pedido pe=(Pedido)listaPedidos.getSelectedValue();
                    combrobar(pe,"editar");

                    verGrafico.setEnabled(false);
                }else {
                    cambiarEstadosComponetesPanel(true,(JPanel)pestanas.getSelectedComponent());
                }
            }
        });
        guardarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Boolean aaaaaaaa=true;
                    k=false;
                    switch (pestanas.getSelectedComponent().getName()){

                        case "Empleados":
                            Empleado em;
                            if(nuevo){
                                em=new Empleado();
                            }else{
                                em=(Empleado) listaEmpleados.getSelectedValue();
                            }
                            em.setTipo(eTipoCombo.getSelectedItem().toString());
                            em.setTelefono(eTelefono.getText());
                            em.setPass(new String(ePass.getPassword()));
                            em.setNombre(eNombre.getText());
                            em.setNick(eNick.getText());
                            em.setEmail(eEmail.getText());
                            em.setApellidos(eApellidos.getText());
                            if(em.getNick().equals("")||em.getPass().equals("")||em.getTipo().equals("")){
                                JOptionPane.showMessageDialog(null, "Error rellene el nick, el pass y el tipo", "Error", JOptionPane.ERROR_MESSAGE);
                                aaaaaaaa=false;
                                break;
                            }
                            if(hibernate.buscarEmpleado(em.getNick()).size()>0&&nuevo){
                                JOptionPane.showMessageDialog(null, "Error ya existe ese nick", "Error", JOptionPane.ERROR_MESSAGE);
                                aaaaaaaa=false;
                            }else{
                                hibernate.guardar(em);
                                modelolistaEmpleados.removeAllElements();
                                for (Empleado ee:hibernate.getEmpleados()) {
                                    modelolistaEmpleados.addElement(ee);
                                }
                            }

                            break;
                        case "Productos":
                            Producto pr;
                            if(nuevo){
                                pr=new Producto();
                            }else{
                                pr=(Producto) listaProductos.getSelectedValue();
                            }
                            pr.setPrecio(Float.parseFloat(prPrecio.getText()));
                            pr.setModelo(prModelo.getText());
                            pr.setStock(Integer.parseInt(prStock.getText()));
                            pr.setMarca(prMarca.getText());
                            pr.setTipo(prTipo.getText());
                            pr.setDescripcion(prDescripcion.getText());
                            if(!temp.equals("")) {
                                try {
                                    pr.setImage(Files.readAllBytes(new File(temp).toPath()));
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                            }
                            if(prPrecio.getText().equals("")||prStock.equals("")||pr.getModelo().equals("")||pr.getMarca().equals("")||pr.getTipo().equals("")){
                                JOptionPane.showMessageDialog(null, "Error se tiene que rellenar los campos", "Error", JOptionPane.ERROR_MESSAGE);
                                aaaaaaaa=false;
                                break;
                            }
                            hibernate.guardar(pr);
                            modelListaProductos.removeAllElements();
                            for (Producto p:hibernate.getProductos()) {
                                modelListaProductos.addElement(p);
                            }
                            break;
                        case "Pedidos":
                            verGrafico.setEnabled(true);
                            Pedido pe;
                            if(nuevo){
                                pe=new Pedido();
                                if(temporal!=null) {
                                    for (DetallePedido jj : temporal) {
                                        jj.setPedido(pe);
                                        Producto p = jj.getProducto();
                                        if (p.getStock() - jj.getCantidad() >= 0) {
                                            p.setStock(p.getStock() - jj.getCantidad());
                                        } else {
                                            p.setStock(0);
                                        }
                                    }
                                    pe.setListaDetallePedido(temporal);
                                }
                            }else{
                                pe=(Pedido) listaPedidos.getSelectedValue();
                            }
                            pe.setFormaPago(pFormaCombo.getSelectedItem().toString());
                            if(pEmpleadoCombo.getSelectedIndex()!=0) {
                                pe.setEmpleado((Empleado) pEmpleadoCombo.getSelectedItem());
                            }if(pClienteCombo.getSelectedIndex()!=0) {
                                pe.setCliente((Cliente) pClienteCombo.getSelectedItem());
                            }
                            pe.setDescuento(Integer.parseInt(pDescuentoCombo.getSelectedItem().toString()));
                            pe.setFecha(pFecha.getDate());
                            pe.setIva(Integer.parseInt(pIvaCombo.getSelectedItem().toString()));
                            pe.setNumero(pNumero.getText());
                            pe.setTs(Float.parseFloat(pSub.getText()));
                            pe.setTt(Float.parseFloat(pTotal.getText()));
                            if(pEmpleadoCombo.getSelectedIndex()==0 ||pClienteCombo.getSelectedIndex()==0 ||temp==null|| pFormaCombo.getSelectedIndex()==0||temporal.size()==0){
                                JOptionPane.showMessageDialog(null, "Error se tiene que selecionar un cliente, un empleado , una forma de pago y productos", "Error", JOptionPane.ERROR_MESSAGE);
                                break;
                            }
                            hibernate.guardar(pe);
                            modeloListaPedidos.removeAllElements();
                            for (Pedido p:hibernate.getPedidos()) {
                                modeloListaPedidos.addElement(p);
                            }
                            temporal=null;
                            break;
                        case "Clientes":
                            Cliente cl;
                            if(nuevo){
                                cl=new Cliente();
                            }else{
                                cl=(Cliente) listaCLientes.getSelectedValue();
                            }
                            cl.setTelefono(cTelefono.getText());
                            cl.setNombre(cNombre.getText());
                            cl.setFecha(cNacimiento.getDate());
                            cl.setEmail(cEmail.getText());
                            cl.setDni(cDni.getText());
                            cl.setDireccion(cDireccion.getText());
                            cl.setApellidos(cApellidos.getText());
                            if(cl.getApellidos().equals("")||cl.getNombre().equals("")){
                                aaaaaaaa=false;
                                JOptionPane.showMessageDialog(null, "Error se tiene que rellenar nombre y el apellido ", "Error", JOptionPane.ERROR_MESSAGE);
                                break;
                            }
                            hibernate.guardar(cl);
                            modeloListaClientes.removeAllElements();
                            for (Cliente c:hibernate.getClientes()) {
                                modeloListaClientes.addElement(c);
                            }
                            break;
                        default:
                            break;
                    }
                    if(aaaaaaaa) {
                        cambiarEstadoBotoneraD();
                        limpiar((JPanel) pestanas.getSelectedComponent());
                        cambiarEstadosComponetesPanel(false, (JPanel) pestanas.getSelectedComponent());
                        nuevo = false;
                        nuevoButton.setText("Nuevo");
                        k = true;
                        temp = "";
                    }
                }catch (NumberFormatException ee){
                    JOptionPane.showMessageDialog(null, "Error tipo de formato incorrecto", "Error", JOptionPane.ERROR_MESSAGE);

                }

            }
        });
        borrarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int a=JOptionPane.showConfirmDialog(null,"¿Seguro ?","confirmacion",JOptionPane.YES_NO_OPTION);
               if(a==JOptionPane.YES_OPTION){
                   switch (pestanas.getSelectedComponent().getName()){
                       case "Empleados":
                           hibernate.eliminar((Empleado) listaEmpleados.getSelectedValue());
                           modelolistaEmpleados.removeAllElements();
                           for (Empleado ee:hibernate.getEmpleados()) {
                               modelolistaEmpleados.addElement(ee);
                           }
                           break;
                       case "Productos":
                           hibernate.eliminar((Producto) listaProductos.getSelectedValue());
                           modelListaProductos.removeAllElements();
                           for (Producto p:hibernate.getProductos()) {
                               modelListaProductos.addElement(p);
                           }

                           break;
                       case "Pedidos":
                           crearFacturaButton.setName("crear");
                           crearFacturaButton.setText("Crear factura");
                           crearAlbaranButton.setName("crear");
                           crearAlbaranButton.setText("Crear Albaran");
                           hibernate.eliminar((Pedido) listaPedidos.getSelectedValue());
                           modeloListaPedidos.removeAllElements();
                           for (Pedido p:hibernate.getPedidos()) {
                               modeloListaPedidos.addElement(p);
                           }
                           break;
                       case "Clientes":
                           hibernate.eliminar((Cliente) listaCLientes.getSelectedValue());
                           modeloListaClientes.removeAllElements();
                           for (Cliente c:hibernate.getClientes()) {
                               modeloListaClientes.addElement(c);
                           }
                           break;
                       default:
                           break;
                   }
               }

                cambiarEstadoBotoneraD();
                limpiar((JPanel)pestanas.getSelectedComponent());
                cambiarEstadosComponetesPanel(false,(JPanel)pestanas.getSelectedComponent());
            }
        });
        anadirProductosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Productos l=new Productos(hibernate);
                temporal=borrarModeloTabla(l.getProductos());
                float subtotal=0;
                for (Producto p:l.getProductos()) {
                    subtotal+=p.getPrecio();
                }
                pSub.setText(subtotal+"");
                calcular();

            }
        });
        crearAlbaranButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Albaran a=null;
                if(crearAlbaranButton.getName().equals("crear")) {
                    verGrafico.setEnabled(true);
                    a = new Albaran();
                    a.setFecha(new Date());
                    a.setNumero(hibernate.nexNumAlbaran() + "");
                    ArrayList<DetalleAlbaran> aa = new ArrayList<>();
                    for (DetallePedido d : ((Pedido) listaPedidos.getSelectedValue()).getListaDetallePedido()) {
                        DetalleAlbaran aaa = new DetalleAlbaran();
                        aaa.setAlbaran(a);
                        aaa.setCantidad(d.getCantidad());
                        aaa.setNombre(d.getNombre());
                        aaa.setPrecio(d.getPrecio());
                        aaa.setProducto(d.getProducto());
                        aa.add(aaa);
                    }
                    a.setListaDetalleAlbaran(aa);
                    Pedido p=(Pedido) listaPedidos.getSelectedValue();
                    a.setPedido(p);
                    p.anadirAlbaran(a);
                    hibernate.guardar(p);
                    modeloListaPedidos.removeAllElements();
                    for (Pedido pp:hibernate.getPedidos()) {
                        modeloListaPedidos.addElement(pp);
                    }
                }
                cambiarEstadoBotoneraC();
                crearFacturaButton.setName("crear");
                crearFacturaButton.setText("Crear factura");
                crearAlbaranButton.setName("crear");
                crearAlbaranButton.setText("Crear Albaran");
                nuevoButton.setText("Nuevo");
                limpiar((JPanel)pestanas.getSelectedComponent());
                cambiarEstadosComponetesPanel(false,(JPanel)pestanas.getSelectedComponent());
                if(a==null) {
                    a = hibernate.unAlbaran((Pedido) listaPedidos.getSelectedValue());
                }
                DataSourceA datasource = new DataSourceA();
                datasource.add(a.getListaDetalleAlbaram());
                try {
                    JasperReport report = (JasperReport) JRLoader.loadObjectFromFile("proyecto.jasper");
                    Map paranetros=new HashMap();
                    paranetros.put("titulo","Albaran");
                    paranetros.put("numero",a.getNumero());
                    paranetros.put("fecha",new SimpleDateFormat("MM-dd-yyyy").format(a.getFecha()));
                    Cliente c=a.getPedido().getCliente();
                    paranetros.put("nombre",c.getNombre()+" "+c.getApellidos());
                    paranetros.put("dni",c.getDni());
                    paranetros.put("direccion",c.getDireccion());
                    //paranetros.put("sub",a.getPedido().getTs());
                    paranetros.put("descuento",a.getPedido().getDescuento());
                    paranetros.put("iva",a.getPedido().getIva());
                    //paranetros.put("total",a.getPedido().getTt());
                    JasperPrint jasperPrint = JasperFillManager.fillReport(report, paranetros, datasource);
                    JasperViewer.viewReport(jasperPrint,false);
                }catch (JRException ee){
                    ee.printStackTrace();
                }

            }
        });
        crearFacturaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Factura f=null;
                if(crearFacturaButton.getName().equals("crear")) {
                    verGrafico.setEnabled(true);
                    f = new Factura();
                    f.setFecha(new Date());
                    Pedido p=(Pedido)listaPedidos.getSelectedValue();
                    Albaran sa=p.getAlbaranes().get(0);
                    f.setAlbaran(sa);
                    f.setNumero(hibernate.nexNumFactura() + "");
                    ArrayList<DetalleFactura> ff = new ArrayList<>();
                    for (DetalleAlbaran a : f.getAlbaran().getListaDetalleAlbaram()) {
                        DetalleFactura fff = new DetalleFactura();
                        fff.setFactura(f);
                        fff.setCantidad(a.getCantidad());
                        fff.setNombre(a.getNombre());
                        fff.setPrecio(a.getPrecio());
                        fff.setProducto(a.getProducto());
                        ff.add(fff);
                    }
                    f.setListaDetalleFactura(ff);

                    sa.anadirFactura(f);
                    hibernate.guardar(p);
                    modeloListaPedidos.removeAllElements();
                    for (Pedido pp:hibernate.getPedidos()) {
                        modeloListaPedidos.addElement(pp);
                    }
                }
                cambiarEstadoBotoneraC();
                crearFacturaButton.setName("crear");
                crearFacturaButton.setText("Crear factura");
                crearAlbaranButton.setName("crear");
                crearAlbaranButton.setText("Crear Albaran");
                nuevoButton.setText("Nuevo");
                limpiar((JPanel)pestanas.getSelectedComponent());
                cambiarEstadosComponetesPanel(false,(JPanel)pestanas.getSelectedComponent());
                try {

                    if(f==null) {
                        f = hibernate.unaFactura(hibernate.unAlbaran((Pedido) listaPedidos.getSelectedValue()));
                    }
                    DataSourceF datasource = new DataSourceF();
                    datasource.add(f.getListaDetalleFactura());
                    JasperReport report = (JasperReport) JRLoader.loadObjectFromFile("proyecto.jasper");
                    Map paranetros=new HashMap();
                    paranetros.put("titulo","Factura");
                    paranetros.put("numero",f.getNumero());
                    paranetros.put("fecha",new SimpleDateFormat("MM-dd-yyyy").format(f.getFecha()));
                    Cliente c=f.getAlbaran().getPedido().getCliente();
                    paranetros.put("nombre",c.getNombre()+" "+c.getApellidos());
                    paranetros.put("dni",c.getDni());
                    paranetros.put("direccion",c.getDireccion());
                    //paranetros.put("sub",f.getAlbaran().getPedido().getTs());
                    paranetros.put("descuento",f.getAlbaran().getPedido().getDescuento());
                    paranetros.put("iva",f.getAlbaran().getPedido().getIva());
                    //paranetros.put("total",f.getAlbaran().getPedido().getTt());
                    JasperPrint jasperPrint = JasperFillManager.fillReport(report, paranetros, datasource);
                    JasperViewer.viewReport(jasperPrint,false);
                }catch (JRException ee){
                    ee.printStackTrace();
                }
            }
        });
        listaPedidos.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                crearFacturaButton.setEnabled(false);
                crearAlbaranButton.setEnabled(false);
                boolean jj=false;
                if(!nuevoButton.getText().equals("Cancelar")&&k && modeloListaPedidos.getSize()>0) {
                    Pedido p = (Pedido) listaPedidos.getSelectedValue();
                    if(p!=null) {
                        pEmpleadoCombo.removeAllItems();
                        pClienteCombo.removeAllItems();
                        pEmpleadoCombo.addItem(p.getEmpleado().toString());
                        pClienteCombo.addItem(p.getCliente().toString());
                        pNumero.setText(p.getNumero());
                        pSub.setText(p.getTs() + "");
                        pTotal.setText(p.getTt() + "");
                        pIvaCombo.setSelectedItem(p.getIva() + "");
                        pClienteCombo.setSelectedItem(p.getCliente().toString());
                        pEmpleadoCombo.setSelectedItem(p.getEmpleado().toString());
                        pFecha.setDate(p.getFecha());
                        pFormaCombo.setSelectedItem(p.getFormaPago());
                        pDescuentoCombo.setSelectedItem(p.getDescuento() + "");
                        jj=combrobar(p,"lista");
                        rellenarProductos(p.getListaDetallePedido());

                        if(!jj) {
                            cambiarEstadoBotoneraC();

                        }
                        borrarButton.setEnabled(true);

                    }
                }
            }
        });
        listaProductos.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(!nuevoButton.getText().equals("Cancelar")) {
                    if (modelListaProductos.getSize() == 0) {

                    } else {
                        Producto p = (Producto) listaProductos.getSelectedValue();
                        if(p!=null) {
                            if (p.getImage() != null) {
                                try {
                                    InputStream in = new ByteArrayInputStream(p.getImage());
                                    BufferedImage image = ImageIO.read(in);
                                    fotol.setIcon(new ImageIcon(image.getScaledInstance(fotol.getWidth(), fotol.getHeight(), Image.SCALE_DEFAULT)));
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                            } else {
                                establecerFotoLabel("camera.png");
                            }
                            prMarca.setText(p.getMarca());
                            prModelo.setText(p.getModelo());
                            prPrecio.setText(p.getPrecio() + "");
                            prStock.setText(p.getStock() + "");
                            prDescripcion.setText(p.getDescripcion());
                            prTipo.setText(p.getTipo());
                            cambiarEstadoBotoneraC();
                        }
                    }
                }
            }
        });
        listaEmpleados.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(!nuevoButton.getText().equals("Cancelar")&& modelolistaEmpleados.getSize()>0) {
                    Empleado ee = (Empleado) listaEmpleados.getSelectedValue();
                    if(ee!=null) {
                        eNombre.setText(ee.getNombre());
                        eApellidos.setText(ee.getApellidos());
                        eNick.setText(ee.getNick());
                        eEmail.setText(ee.getEmail());
                        ePass.setText(ee.getPass());
                        eTelefono.setText(ee.getTelefono());
                        eTipoCombo.setSelectedItem(ee.getTipo());
                        cambiarEstadoBotoneraC();
                    }
                }
            }
        });
        listaCLientes.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(!nuevoButton.getText().equals("Cancelar")&&modeloListaClientes.getSize()>0) {
                    Cliente c = (Cliente) listaCLientes.getSelectedValue();
                    if(c!=null) {
                        cNombre.setText(c.getNombre());
                        cApellidos.setText(c.getApellidos());
                        cDireccion.setText(c.getDireccion());
                        cDni.setText(c.getDni());
                        cEmail.setText(c.getEmail());
                        cTelefono.setText(c.getTelefono());
                        cNacimiento.setDate(c.getFecha());
                        cambiarEstadoBotoneraC();
                    }
                }
            }
        });
        eBuscar.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {


            }

            @Override
            public void keyPressed(KeyEvent e) {


            }

            @Override
            public void keyReleased(KeyEvent e) {
                modelolistaEmpleados.removeAllElements();
                ArrayList<Empleado> em;
                if(eBuscar.getText().equals("")){
                    em=hibernate.getEmpleados();
                }else {
                    em = hibernate.buscarEmpleado(eBuscar.getText());
                }

                for (Empleado ee : em) {
                    modelolistaEmpleados.addElement(ee);
                }

            }
        });
        cBuscar.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                modeloListaClientes.removeAllElements();
                ArrayList<Cliente> em;
                if(cBuscar.getText().equals("")){
                    em=hibernate.getClientes();
                }else {
                    em = hibernate.buscarCliente(cBuscar.getText());
                }
                for (Cliente ee : em) {
                    modeloListaClientes.addElement(ee);
                }
            }
        });
        prBuscar.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                modelListaProductos.removeAllElements();
                ArrayList<Producto> em;
                if(prBuscar.getText().equals("")){
                    em=hibernate.getProductos();
                }else {
                    em=hibernate.buscarProducto(prBuscar.getText());
                }
                for (Producto ee : em) {
                    modelListaProductos.addElement(ee);
                }
            }
        });
        pBucar.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                modeloListaPedidos.removeAllElements();
                ArrayList<Pedido> em;
                if(pBucar.getText().equals("")){
                    em=hibernate.getPedidos();
                }else {
                    em = hibernate.buscarPedido(pBucar.getText());
                }
                for (Pedido ee : em) {
                    modeloListaPedidos.addElement(ee);
                }
            }
        });
    }
    public boolean combrobar(Pedido p,String r){
        boolean a=false;
        if(p.getAlbaranes()!=null && p.getAlbaranes().size()>0){
            crearAlbaranButton.setName("visualizar");
            crearAlbaranButton.setText("Visualizar alabaran");
            crearAlbaranButton.setEnabled(true);
            if(p.getAlbaranes().get(0).getFacturas()!=null && p.getAlbaranes().get(0).getFacturas().size()>0){
                crearFacturaButton.setName("visualizar");
                crearFacturaButton.setText("Visualizar factura");
                crearFacturaButton.setEnabled(true);
                a=true;


            }else{
                crearFacturaButton.setName("crear");
                crearFacturaButton.setText("Crear factura");
                if(r.equals("editar")) {
                    crearFacturaButton.setEnabled(true);
                }else if(r.equals("lista")){
                    crearFacturaButton.setEnabled(false);
                }
            }
        }else{
            crearFacturaButton.setName("crear");
            crearFacturaButton.setText("Crear factura");
            crearFacturaButton.setEnabled(false);
            crearAlbaranButton.setName("crear");
            crearAlbaranButton.setText("Crear Albaran");
            if(r.equals("lista")){
                crearAlbaranButton.setEnabled(false);
            }else if(r.equals("editar")){
                crearAlbaranButton.setEnabled(true);
            }


        }
        return a;
    }
    /**
     * cierra la ventana y cierra la sesion de hibernate
     */
    public void cerrar(){
        hibernate.desconectar();
        System.exit(0);
    }

    /**
     * Recibe un panl y recorrer los elementos de este para limpiarlos
     * @param panel
     */
    public void limpiar( JPanel panel){
        if(panel!=null) {
            if(panel.getName().equals("Pedidos")){
                pProductosTabla.setModel(new DefaultTableModel());
            }
            for (Component c : panel.getComponents()) {
                if (c instanceof JLabel || c instanceof JTextField || c instanceof JPasswordField || c instanceof JComboBox || c instanceof JTextArea  || c instanceof JDateChooser) {
                    if (c instanceof JLabel) {
                        if (c == fotol) {
                            establecerFotoLabel("camera.png");
                        }
                    } else if (c instanceof JTextField) {
                        ((JTextField) c).setText("");
                    }else if (c instanceof JTextArea) {
                        ((JTextArea) c).setText("");

                    }else if (c instanceof JPasswordField) {
                        ((JPasswordField) c).setText("");

                    }else if (c instanceof JComboBox) {
                        try {
                            ((JComboBox) c).setSelectedIndex(0);
                        }catch (IllegalArgumentException ww) {

                        }

                    }else if(c instanceof JDateChooser){
                        ((JDateChooser) c).setDate(null);

                    }


                }
            }
        }

    }

    /**
     * reccore los elementos de un panel y los habilita o deshabilita segun sea necesario
     * @param a
     * @param panel
     */
    public void cambiarEstadosComponetesPanel(boolean a, JPanel panel){
        if(panel!=null) {
            for (Component c : panel.getComponents()) {
                if (c instanceof JButton || c instanceof JTextField || c instanceof JPasswordField || c instanceof JComboBox || c instanceof JTextArea) {

                    if (c instanceof JTextField) {
                        if(c.getName()!=null) {
                            if (c.getName().equals("buscar")) {

                            } else if (c.getName().equals("a")) {
                                ((JTextField) c).setEditable(false);
                            }
                        }else {
                            ((JTextField) c).setEditable(a);
                        }
                    } else if (c instanceof JComboBox) {
                        ((JComboBox) c).setEditable(a);
                        ((JComboBox) c).setEnabled(a);
                    } else if (c instanceof JTextArea) {
                        ((JTextArea) c).setEditable(a);
                    } else if (c instanceof JPasswordField) {
                        ((JPasswordField) c).setEditable(a);
                    } else if(c instanceof JButton ){

                        if (c.getName().equals("grafico")) {

                        } else{
                            c.setEnabled(a);
                        }
                    }else{
                        c.setEnabled(a);
                    }
                } else if (c instanceof JDateChooser) {
                    c.setEnabled(a);
                } else if (c == fotol) {
                    if (a) {
                        c.addMouseListener(this);
                    } else {
                        c.removeMouseListener(this);
                    }

                }
            }
        }

    }

    /**
     * cambia los estados del los botones del panel de abajo segun correponda
     */

    public void cambiarEstadoBotoneraA(){
        nuevoButton.setEnabled(true);
        editarButton.setEnabled(false);
        borrarButton.setEnabled(false);
        guardarButton.setEnabled(true);
    }
    /**
     * cambia los estados del los botones del panel de abajo segun correponda
     */
    public void cambiarEstadoBotoneraD(){
        nuevoButton.setEnabled(true);
        editarButton.setEnabled(false);
        borrarButton.setEnabled(false);
        guardarButton.setEnabled(false);
    }
    /**
     * cambia los estados del los botones del panel de abajo segun correponda
     */
    public void cambiarEstadoBotoneraB(){
        nuevoButton.setEnabled(false);
        editarButton.setEnabled(true);
        borrarButton.setEnabled(true);
        guardarButton.setEnabled(true);
    }
    /**
     * cambia los estados del los botones del panel de abajo segun correponda
     */
    public void cambiarEstadoBotoneraC(){
        nuevoButton.setEnabled(true);
        editarButton.setEnabled(true);
        borrarButton.setEnabled(true);
        guardarButton.setEnabled(false);
    }

    /**
     * lisener del raton para controlar si se presionan unas labels
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if(pestanas.getSelectedComponent().getName().equals("Productos")) {
            JFileChooser foto = new JFileChooser();
            foto.setFileFilter(new FileNameExtensionFilter("PNG",  "png"));
            if (foto.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                temp=foto.getSelectedFile().getPath();
                establecerFotoLabel(foto.getSelectedFile().toString());
            }
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {
        if(pestanas.getSelectedComponent().getName().equals("Empleados")) {
            punto=ePass.getEchoChar();
            ePass.setEchoChar((char)0);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if(pestanas.getSelectedComponent().getName().equals("Empleados")) {
            ePass.setEchoChar(punto);
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    /**
     * cambia el icono de la label
     * @param path
     */
    public void establecerFotoLabel(String path){
        ImageIcon icon = new ImageIcon(path);
        Icon icono = new ImageIcon(icon.getImage().getScaledInstance(fotol.getWidth(), fotol.getHeight(), Image.SCALE_DEFAULT));
        fotol.setIcon(icono);
    }

    /**
     * Recibe un array con los productos y elimina los dato anteriores y los sustituye por lo snuevos  y retorna los los detalles del pedido
     * @param productos
     * @return
     */
    public ArrayList<DetallePedido> borrarModeloTabla(ArrayList<Producto> productos){
        ArrayList<DetallePedido>detallePedidos=new ArrayList<>();
        modeloTabla=new DefaultTableModel();
        modeloTabla.addColumn("Marca");
        modeloTabla.addColumn("Modelo");
        modeloTabla.addColumn("Precio");
        modeloTabla.addColumn("Cantidad");

        if(productos!=null) {
            if(productos.size()>1){
                String numeros="";
                for (int j =0;j<productos.size();j++) {
                    boolean w = false;

                    Producto p = productos.get(j);
                    int num = 1;
                    for (int i = j + 1; i < productos.size(); i++) {
                        if (p == productos.get(i)) {
                            num++;

                            if (numeros.equals("")) {
                                numeros = i + "";
                            } else {
                                numeros += "," + i;
                            }
                        }
                    }
                    for (String k : numeros.split(",")) {
                        if (j == Integer.parseInt(k)) {

                            w = true;
                        }
                    }
                    if (!w) {
                        modeloTabla.addRow(new Object[]{p.getMarca(), p.getModelo(), p.getPrecio(), num});
                        DetallePedido dp = new DetallePedido();
                        dp.setCantidad(num);
                        dp.setPrecio(p.getPrecio());
                        dp.setNombre(p.getMarca() + " " + p.getModelo());
                        dp.setProducto(p);
                        detallePedidos.add(dp);
                    }
                }


            }else{
                modeloTabla.addRow(new Object[]{productos.get(0).getMarca(),productos.get(0).getModelo(), productos.get(0).getPrecio(),1});

                DetallePedido dp = new DetallePedido();
                dp.setCantidad(1);
                dp.setPrecio( productos.get(0).getPrecio());
                dp.setNombre(productos.get(0).getMarca() + " " +productos.get(0).getModelo());
                dp.setProducto(productos.get(0));
                detallePedidos.add(dp);
            }
        }
        pProductosTabla.setModel(modeloTabla);
        return detallePedidos;
    }

    /**
     * pinta los detalles del pedido en la tabla
     * @param productos
     */
    public void rellenarProductos(List<DetallePedido> productos){
        modeloTabla=new DefaultTableModel();
        modeloTabla.addColumn("Marca");
        modeloTabla.addColumn("Modelo");
        modeloTabla.addColumn("Precio");
        modeloTabla.addColumn("Cantidad");
        for(DetallePedido d:productos){
            modeloTabla.addRow(new Object[]{d.getNombre().split(" ")[0],d.getNombre().split(" ")[1], d.getPrecio(),d.getCantidad()});
        }
        pProductosTabla.setModel(modeloTabla);
    }
    @Override
    public void actionPerformed(ActionEvent e) {

    }

    /**
     * clacula los datos del pedido
     */
    public void calcular(){
        float sub=Float.parseFloat(pSub.getText());
        float descuento=Float.parseFloat(pDescuentoCombo.getSelectedItem().toString())/Float.parseFloat("100");
        float subDe=sub-(descuento*sub);
        float iva=Float.parseFloat(pIvaCombo.getSelectedItem().toString());
        float total=subDe+((iva/Float.parseFloat("100"))*subDe);
        pTotal.setText(total+"");

    }

}
