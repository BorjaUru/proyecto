package com.bulsa.proyecto.gui;

import javax.swing.*;
import java.awt.event.*;

/**
 * Dialogo que se muestra al inicar la aplicacion para hacer login
 */

public class Login extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField textField1;
    private JPasswordField passwordField1;

    private String usuario;
    private String pass;
    private boolean cancelar;

    public boolean isCancelar() {
        return cancelar;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getPass() {
        return pass;
    }

    /**
     * inicializa los linseners de los botones y el de cerrar el dialogo
     */
    public Login() {
        cancelar=false;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

    }

    /**
     * contiene las instrucions que tiene que realizar el programa cuando le das a login
     */

    private void onOK() {
        usuario=textField1.getText();
        pass=new String(passwordField1.getPassword());
        dispose();
    }
    /**
     * contiene las instrucions que tiene que realizar el programa cuando cancelas el login
     */
    private void onCancel() {
        cancelar=true;
        dispose();
    }
}
