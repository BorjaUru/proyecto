package com.bulsa.proyecto.gui;


import com.bulsa.proyecto.base.*;
import com.bulsa.proyecto.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.ArrayList;


/**
 * Created by BORJA on 06/04/2017.
 * clase hibernate que gestiona la base de datos añade, modifica, elimina o recoge informacion de esta.
 */
public class Hibernate {

    /**
     * crea la conexion
     */
    public void conectar() {
        try {
            HibernateUtil.buildSessionFactory();
            HibernateUtil.openSession();

        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    /**
     * desconecta la sesion
     */

    public void desconectar() {
        try {
            HibernateUtil.closeSessionFactory();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    /**
     * comprrueba si existe un usuario con el nic y el pass suministrado
     * @param nick el nick a comprobar si existe
     * @param pass el pass a comprobar si existe
     * @return un empledo
     */
    public Empleado login(String nick ,String pass){
        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Empleado  e WHERE e.nick = :nick AND e.pass = :pass");

        query.setParameter("nick", nick);
        query.setParameter("pass", pass);
        Empleado e = (Empleado) query.uniqueResult();



        return e;
    }

    /**
     * retorna los empleados de la base de datos
     * @return empleados
     */
    public ArrayList<Empleado> getEmpleados() {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Empleado");
        ArrayList<Empleado> empleados = (ArrayList<Empleado>) query.list();

        return empleados;
    }

    /**
     * retorna los clientes de la base de datos
     * @return clientes
     */
    public ArrayList<Cliente> getClientes() {

        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Cliente");
        ArrayList<Cliente> clientes = (ArrayList<Cliente>) query.list();
        if(clientes==null){
            clientes=new ArrayList<>();
        }

        return clientes;
    }

    /**
     * retorna los productos de la base de datos
     * @return productos
     */
    public ArrayList<Producto> getProductos() {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Producto");
        ArrayList<Producto> productos = (ArrayList<Producto>) query.list();

        return productos;
    }

    /**
     * retorna los productos que tengan stock
     * @return productos
     */
    public ArrayList<Producto> getProductoss() {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Producto  WHERE stock>0 ");
        ArrayList<Producto> productos = (ArrayList<Producto>) query.list();

        return productos;
    }

    /**
     * retorna pedidos de la base de datos
     * @return pedidos
     */
    public ArrayList<Pedido> getPedidos() {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Pedido");
        ArrayList<Pedido> pedidos = (ArrayList<Pedido>) query.list();

        return pedidos;
    }

    /**
     * guarda o modifica un empledado
     * @param em empleado a guardar o modificar
     */
    public void guardar(Empleado em){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(em);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * guarda un producto o lo modifica
     * @param pr producto a modificar o guardar
     */
    public void guardar(Producto pr){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(pr);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * guarda o modifica un cliente
     * @param cl
     */
    public void guardar(Cliente cl){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(cl);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * guarda o modifia un pedido
     * @param pe
     */
    public void guardar(Pedido pe){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(pe);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * guarda o modifica un albaran
     * @param albaran
     */
    public void guardar(Albaran albaran){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(albaran);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * guarda o modifica una factura
     * @param f
     */
    public void guardar(Factura f){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(f);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * elimina un empleado
     * @param em
     */
    public void eliminar(Empleado em){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(em);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * elimina un producto
     * @param pr
     */
    public void eliminar(Producto pr){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(pr);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * elimina un cliente
     * @param cl
     */
    public void eliminar(Cliente cl){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(cl);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * elimina un pedido
     * @param pe
     */
    public void eliminar(Pedido pe){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(pe);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * busca cliente spor nombre
     * @param nombre
     * @return cliente
     */
    public ArrayList<Cliente> buscarCliente(String nombre ){
        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Cliente c WHERE c.nombre = :nombre ");
        query.setParameter("nombre", nombre);

        ArrayList<Cliente> clientes = (ArrayList<Cliente>) query.list();
        return clientes;
    }

    /**
     * busca empleados por un nick
     * @param nick
     * @return empleados
     */
    public ArrayList<Empleado> buscarEmpleado(String nick ){
        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Empleado e WHERE e.nick = :nick ");
        query.setParameter("nick", nick);

        ArrayList<Empleado> e = (ArrayList<Empleado>) query.list();


        return e;
    }



    /**
     * busca productos por una marca
     * @param marca
     * @return productos
     */
    public ArrayList<Producto> buscarProducto(String marca){
        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Producto p WHERE p.marca = :marca ");
        query.setParameter("marca", marca);

        ArrayList<Producto> p = (ArrayList<Producto>) query.list();
        return p;
    }

    /**
     * busca por numero de pedido en pedidos
     * @param num
     * @return lista de pedidos
     */
    public ArrayList<Pedido> buscarPedido(String num){
        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Pedido p WHERE p.numero = :num ");
        query.setParameter("num", num);

        ArrayList<Pedido> p = (ArrayList<Pedido>) query.list();
        return p;
    }

    /**
     * retonrna el next numero de pedido
     * @return num
     */
    public int nexNumPedido(){

        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Pedido ORDER BY id DESC");
        query.setMaxResults(1);
        Pedido p = (Pedido) query.uniqueResult();

        if(p==null){
            return 1;
        }else{

            return Integer.parseInt(p.getNumero())+1;
        }

    }

    /**
     * retorna el next numero de albaran
     * @return num
     */
    public int nexNumAlbaran(){

        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Albaran ORDER BY id DESC");
        query.setMaxResults(1);
        Albaran a = (Albaran) query.uniqueResult();

        if(a==null){
            return 1;
        }else{

            return Integer.parseInt(a.getNumero())+1;
        }

    }

    /**
     * retonar el next numero de factura
     * @return numero
     */
    public int nexNumFactura(){

        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Factura ORDER BY id DESC");
        query.setMaxResults(1);
        Factura f = (Factura) query.uniqueResult();

        if(f==null){
            return 1;
        }else{

            return Integer.parseInt(f.getNumero())+1;
        }

    }

    /**
     * retorna el albaran que este relacionado con un pedido
     * @param pedido
     * @return albaran
     */
    public Albaran unAlbaran(Pedido pedido){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Albaran where pedido=:pedido");
        query.setParameter("pedido", pedido);
        query.setMaxResults(1);
        return (Albaran) query.uniqueResult();
    }

    /**
     * retorna la factura que este relacionada con el albaran pasado como parametro
     * @param albaran
     * @return factura
     */
    public Factura unaFactura(Albaran albaran){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Factura where albaran=:albaran");
        query.setParameter("albaran", albaran);
        query.setMaxResults(1);
        return (Factura) query.uniqueResult();
    }

    /**
     * retorna los pedidos ordenados por empleados
     * @return pedidos ordenados
     */
    public ArrayList<Pedido> getPedidoss() {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Pedido order by empleado desc");
        ArrayList<Pedido> pedidos = (ArrayList<Pedido>) query.list();

        return pedidos;
    }
}
