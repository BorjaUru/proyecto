package com.bulsa.proyecto.gui;

import com.bulsa.proyecto.base.Producto;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Muestra un dialogo para añadir productos a un pedido
 */

public class Productos extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JComboBox comboBox1;
    private JTable table1;
    private JButton anadirButton;
    private JButton eliminarButton;
    private Hibernate h;
    private ArrayList<Producto>productos;
    private DefaultTableModel modeloTabla;

    public ArrayList<Producto> getProductos() {
        return productos;
    }


    /**
     * rebibe el objeto de hibernate para sacar lo sproductos de la base de datos que tengan stock y contiene los lisener para añadir productos a la tabla o quitarlos
     * @param h
     */
    public Productos(Hibernate h) {

        productos=new ArrayList<>();
        modeloTabla=new DefaultTableModel();
        modeloTabla.addColumn("Marca");
        modeloTabla.addColumn("Modelo");
        modeloTabla.addColumn("Precio");
        table1.setModel(modeloTabla);
        ListSelectionModel s=null;
        table1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        s=table1.getSelectionModel();
        s.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                eliminarButton.setEnabled(true);
            }
        });
        this.h=h;
        for(Producto p:h.getProductoss()){
            comboBox1.addItem(p);
        }
        if(comboBox1.getItemCount()==0){
            JOptionPane.showMessageDialog(null, "Error no existen productos con stock para añadir", "Error", JOptionPane.ERROR_MESSAGE);

        }
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        anadirButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(comboBox1.getItemCount()>0) {
                    Producto p = (Producto) comboBox1.getSelectedItem();
                    productos.add(p);
                    modeloTabla.addRow(new Object[]{p.getMarca(), p.getModelo(), p.getPrecio()});
                }

            }
        });
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modeloTabla.removeRow(table1.getSelectedRow());
                eliminarButton.setEnabled(false);

            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });


        setLocationRelativeTo(null);
        pack();
        setVisible(true);

    }

    /**
     * contiene los pasos cuando se guardan los productos
     */
    private void onOK() {
        // add your code here
        dispose();
    }
    /**
     * contiene los pasos cuando se cierra el dialog o se le da a cancelar
     */
    private void onCancel() {
        // add your code here if necessary
        productos=new ArrayList<>();
        dispose();
    }
}
