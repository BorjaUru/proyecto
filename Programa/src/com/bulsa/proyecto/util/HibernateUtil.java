package com.bulsa.proyecto.util;

import com.bulsa.proyecto.base.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Clase que gestiona hibernate
 */

public class HibernateUtil {

  private static SessionFactory sessionFactory;
  private static Session session;
	
  /**
   * Crea la factoria de sesiones
   */
  public static void buildSessionFactory() {

    Configuration configuration = new Configuration();
    configuration.configure();

    configuration.addAnnotatedClass(Cliente.class);
    configuration.addAnnotatedClass(Empleado.class);
    configuration.addAnnotatedClass(Pedido.class);
    configuration.addAnnotatedClass(Producto.class);
    configuration.addAnnotatedClass(Albaran.class);
    configuration.addAnnotatedClass(Factura.class);
    configuration.addAnnotatedClass(DetallePedido.class);
    configuration.addAnnotatedClass(DetalleAlbaran.class);
    configuration.addAnnotatedClass(DetalleFactura.class);
    ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
      configuration.getProperties()).build();
    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
  }
	
  /**
   * Abre una nueva sesión
   */
  public static void openSession() {
    session = sessionFactory.openSession();
  }
	
  /**
   * Devuelve la sesión actual
   * @return
   */
  public static Session getCurrentSession() {
	
    if ((session == null) || (!session.isOpen()))
      openSession();
			
    return session;
  }
	
  /**
   * Cierra Hibernate
   */
  public static void closeSessionFactory() {
	
    if (session != null)
      session.close();
		
    if (sessionFactory != null)
      sessionFactory.close();
  }
}